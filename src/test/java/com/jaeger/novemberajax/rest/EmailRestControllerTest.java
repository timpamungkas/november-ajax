package com.jaeger.novemberajax.rest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaeger.novemberajax.rest.model.Email;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class EmailRestControllerTest {

	private static final String ENDPOINT_BLAST = "/v1/email/blast";
	private static final String ENDPOINT_SEND = "/v1/email/send";

	private static ObjectMapper objectMapper;
	private static List<String> tos;
	private static List<String> ccs;
	private static List<String> bccs;
	private static String testSubject;
	private static String testHtmlContent;

	@BeforeAll
	static final void setup() {
		objectMapper = new ObjectMapper();

		tos = new ArrayList<String>();
		ccs = new ArrayList<String>();
		bccs = new ArrayList<String>();

		tos.add("timotius.pamungkas@gmail.com");
		ccs.add("timotius.pamungkas@gmail.com");
		bccs.add("timotius.pamungkas@gmail.com");

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date now = new Date();

		testSubject = "Test Email Controller " + df.format(now);
		testHtmlContent = "<h1>Test Email Controller (HTML Controller) " + df.format(now) + "</h1>"
				+ "<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2>"
				+ "<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
				+ "<strong>Ut enim ad minim veniam, </strong>" + "quis nostrud exercitation ullamco laboris nisi ut "
				+ "<em>aliquip ex ea commodo consequat. </em>"
				+ "Duis aute irure dolor in reprehenderit in voluptate velit esse <sub>cillum dolore eu </sub>"
				+ "fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, "
				+ "sunt in culpa qui officia deserunt mollit anim id est laborum.</p>"
				+ "<p>Sed ut perspiciatis, unde omnis iste natus error sit "
				+ "<sup>voluptatem accusantium doloremque laudantium, </sup>" + "totam rem aperiam eaque ipsa, "
				+ "quae ab illo inventore veritatis et quasi architecto beatae vitae dicta del veritas.</p>";
	}

	@Autowired
	private MockMvc mockMvc;

	@Test
	final void testBlastEmail_InvalidRecipientNum() throws Exception {
		var email = new Email(tos, ccs, bccs, testSubject, testHtmlContent);
		var invalidTos = new ArrayList<String>();

		for (int i = 0; i <= Email.MAX_EMAIL_NUM_RECIPIENT; i++) {
			invalidTos.add(RandomStringUtils.randomAlphabetic(8, 21) + "@someinvalidsites.xyz");
		}

		email.setTos(invalidTos);

		var jsonString = objectMapper.writeValueAsString(email);
		var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_BLAST).content(jsonString)
				.contentType(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

		mockMvc.perform(requestBuilder).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("header")))
				.andExpect(content().string(containsString("errors")))
				.andExpect(content().string(containsString("process_time")));
	}

	@Test
	final void testBlastEmail_InvalidSubject() throws Exception {
		// subject length min 1 - max 200 chars
		var invalidEmails = List.of(
				new Email(tos, ccs, bccs, RandomStringUtils.randomAlphanumeric(201), testHtmlContent),
				new Email(tos, ccs, bccs, StringUtils.EMPTY, testHtmlContent),
				new Email(tos, ccs, bccs, null, testHtmlContent));

		for (var email : invalidEmails) {
			var jsonString = objectMapper.writeValueAsString(email);
			var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_BLAST).content(jsonString)
					.contentType(MediaType.APPLICATION_JSON)
					.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

			mockMvc.perform(requestBuilder).andExpect(status().isBadRequest())
					.andExpect(content().string(containsString("header")))
					.andExpect(content().string(containsString("errors")))
					.andExpect(content().string(containsString("process_time")));
		}
	}

	@Test
	final void testBlastEmail_ValidEmail() throws Exception {
		var email = new Email(tos, ccs, bccs, testSubject, testHtmlContent);
		var jsonString = objectMapper.writeValueAsString(email);
		var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_BLAST).content(jsonString)
				.contentType(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

		mockMvc.perform(requestBuilder).andExpect(status().is2xxSuccessful())
				.andExpect(content().string(containsString("header")))
				.andExpect(content().string(containsString("data")))
				.andExpect(content().string(containsString("process_time")));
	}

	@Test
	final void testSendEmail_InvalidPayloadNum() throws Exception {
		var emails = new ArrayList<Email>();

		// max payload is 20 emails
		for (int i = 0; i <= 20; i++) {
			emails.add(new Email(tos, ccs, bccs, testSubject, testHtmlContent));
		}

		var jsonString = objectMapper.writeValueAsString(emails);
		var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_SEND).content(jsonString)
				.contentType(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

		mockMvc.perform(requestBuilder).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("header")))
				.andExpect(content().string(containsString("errors")))
				.andExpect(content().string(containsString("process_time")));
	}

	@Test
	final void testSendEmail_InvalidSubject() throws Exception {
		// subject length min 1 - max 200 chars
		var invalidEmails = List.of(
				new Email(tos, ccs, bccs, RandomStringUtils.randomAlphanumeric(201), testHtmlContent),
				new Email(tos, ccs, bccs, StringUtils.EMPTY, testHtmlContent),
				new Email(tos, ccs, bccs, null, testHtmlContent));

		for (var email : invalidEmails) {
			var emailToSend = new ArrayList<>();
			emailToSend.add(email);

			var jsonString = objectMapper.writeValueAsString(emailToSend);
			var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_SEND).content(jsonString)
					.contentType(MediaType.APPLICATION_JSON)
					.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

			mockMvc.perform(requestBuilder).andExpect(status().isBadRequest())
					.andExpect(content().string(containsString("header")))
					.andExpect(content().string(containsString("errors")))
					.andExpect(content().string(containsString("process_time")));
		}
	}

	@Test
	final void testSendEmail_ValidEmail() throws Exception {
		var emails = List.of(new Email(tos, ccs, bccs, testSubject, testHtmlContent));
		var jsonString = objectMapper.writeValueAsString(emails);
		var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_SEND).content(jsonString)
				.contentType(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

		mockMvc.perform(requestBuilder).andExpect(status().is2xxSuccessful())
				.andExpect(content().string(containsString("header")))
				.andExpect(content().string(containsString("data")))
				.andExpect(content().string(containsString("process_time")));
	}

}
