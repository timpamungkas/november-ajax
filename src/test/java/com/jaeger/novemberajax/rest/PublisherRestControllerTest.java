package com.jaeger.novemberajax.rest;

import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URL;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class PublisherRestControllerTest {

	private static final String ENDPOINT_PUBLISH = "/v1/publish/x.pork.work/q.pork.fried/";

	private static ObjectMapper objectMapper;

	private static String genericJson;

	@BeforeAll
	static void setup() throws JsonProcessingException {
		objectMapper = new ObjectMapper();

		var messageBody = new HashMap<String, String>();
		for (var i = 0; i < 5; i++) {
			messageBody.put("key" + i, "value" + i);
		}

		// simple map
		genericJson = objectMapper.writeValueAsString(messageBody);
	}

	@Autowired
	private MockMvc mockMvc;

	@Test
	final void testNotJsonBody() throws Exception {
		for (var mime : DefaultRestControllerTest.notJsonMediaTypes) {
			// valid json string, but not json content type
			var genericRequestBuilder = MockMvcRequestBuilders.post(ENDPOINT_PUBLISH).contentType(mime)
					.content(genericJson).header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

			mockMvc.perform(genericRequestBuilder).andExpect(status().isUnsupportedMediaType());
		}
	}

	@Test
	final void testPublishToExistingExchange_InvalidJson() throws Exception {
		var invalidJsons = List.of("{this is absolutely not a valid json}", "{key\": \"value}",
				"{ \"color\" : \"Red\", \"type\" \"Ford\" }", "{\"key\":\"value\"",
				"{\"color : \"Silver\", \"type\" : \"Honda\"}", "{a: tail}", "[\"key\":\"value\"]");

		for (var invalidJson : invalidJsons) {
			var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_PUBLISH).contentType(MediaType.APPLICATION_JSON)
					.content(invalidJson).header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

			mockMvc.perform(requestBuilder).andExpect(status().isBadRequest());
		}
	}

	@Test
	final void testPublishToExistingExchange_ValidJson() throws Exception {
		var genericRequestBuilder = MockMvcRequestBuilders.post(ENDPOINT_PUBLISH)
				.contentType(MediaType.APPLICATION_JSON).content(genericJson)
				.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

		mockMvc.perform(genericRequestBuilder).andExpect(status().is2xxSuccessful());

		var validJsonUrls = List.of(
				"https://gist.githubusercontent.com/planetoftheweb/8bc1698e423ef0ff3db734f4115bf214/raw/27d4811f120b784ca9e35182419cffea377439b7/data.json",
				"https://raw.githubusercontent.com/sitepoint-editors/json-examples/master/src/products.json",
				"https://raw.githubusercontent.com/sitepoint-editors/json-examples/master/src/youtube-search-results.json",
				"https://raw.githubusercontent.com/LearnWebCode/json-example/master/pets-data.json",
				"https://raw.githubusercontent.com/LearnWebCode/json-example/master/animals-1.json",
				"https://raw.githubusercontent.com/LearnWebCode/json-example/master/animals-2.json");

		for (var s : validJsonUrls) {
			JsonNode jsonNode = objectMapper.readTree(new URL(s));
			assertNotNull(jsonNode);
			final var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_PUBLISH)
					.contentType(MediaType.APPLICATION_JSON).content(jsonNode.toString())
					.header(HttpHeaders.AUTHORIZATION, DefaultRestControllerTest.basicAuthString);

			assertTimeout(ofMillis(1000), () -> {
				mockMvc.perform(requestBuilder).andExpect(status().is2xxSuccessful());
			});
		}
	}

	@Test
	final void testUnauthorizedAccess() throws Exception {
		var requestBuilder = MockMvcRequestBuilders.post(ENDPOINT_PUBLISH).contentType(MediaType.APPLICATION_JSON)
				.content(genericJson);

		mockMvc.perform(requestBuilder).andExpect(status().isUnauthorized());
	}

}
