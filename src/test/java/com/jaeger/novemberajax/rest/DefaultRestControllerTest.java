package com.jaeger.novemberajax.rest;

import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.Base64Utils;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class DefaultRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	static final String basicAuthString = "Basic " + Base64Utils.encodeToString("dev:password".getBytes());
	static final List<MediaType> notJsonMediaTypes = List.of(MediaType.APPLICATION_FORM_URLENCODED,
			MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.IMAGE_GIF, MediaType.IMAGE_JPEG,
			MediaType.IMAGE_PNG, MediaType.MULTIPART_FORM_DATA, MediaType.TEXT_HTML, MediaType.TEXT_MARKDOWN,
			MediaType.TEXT_PLAIN, MediaType.TEXT_XML);

	@Test
	final void testPing() throws Exception {
		var endpoints = List.of("/", "/health", "/ping");

		for (var e : endpoints) {
			final var requestBuilder = MockMvcRequestBuilders.get(e);

			assertTimeout(ofMillis(500), () -> {
				mockMvc.perform(requestBuilder).andExpect(status().isOk());
			});
		}
	}

}
