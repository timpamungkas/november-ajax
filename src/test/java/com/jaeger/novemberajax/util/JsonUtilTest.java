package com.jaeger.novemberajax.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;

import org.junit.jupiter.api.Test;

class JsonUtilTest {

	@Test
	final void testConstructor() {
		var util = new JsonUtil();
		assertNotNull(util);
	}

	@Test
	final void testIsValidJson() {
		var invalidJson = List.of("{\"color : \"Silver\", \"type\" : \"Honda\"",
				"color\"\" : \"Silver\", \"type\" : Toyota\"}");
		var validJson = List.of("{\"color\" : \"Silver\", \"type\" : \"Honda\"}",
				"{\"color\" : \"Black\", \"type\" : \"Toyota\"}");

		for (String s : invalidJson) {
			assertFalse(JsonUtil.isValidJson(s), s);
		}

		for (String s : validJson) {
			assertTrue(s, JsonUtil.isValidJson(s));
		}
	}

}
