package com.jaeger.novemberajax.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.springframework.stereotype.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.jaeger.novemberajax.service.QrCodeGenerator;

@Service
public class ZxingQrCodeGenerator implements QrCodeGenerator {

	@Override
	public byte[] getQrCode(String text, int size, ImageFormat imageFormat) throws IOException {
		try {
			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, size, size);

			ByteArrayOutputStream imageOutputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, imageFormat.toString(), imageOutputStream);
			byte[] pngData = imageOutputStream.toByteArray();
			return pngData;
		} catch (WriterException e) {
			throw new IOException(e.getMessage());
		}
	}

}
