package com.jaeger.novemberajax.service.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.ChannelCallback;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP.Exchange.DeclareOk;
import com.rabbitmq.client.Channel;

@Service
public class RabbitmqPublisher {

	private static ObjectMapper objectMapper = new ObjectMapper();

	private static final Logger log = LoggerFactory.getLogger(RabbitmqPublisher.class);

	@Autowired
	private RabbitTemplate rabbitTemplate;

	/**
	 * Publish message
	 * 
	 * @param message any string
	 * @return true if published successfully. <strong>WARNING: </strong> As per
	 *         AMQP specification, <i>messages are silently dropped, by default, if
	 *         they can't be routed. One of the ways RabbitMQ gets its amazing
	 *         performance is the sending thread is not blocked waiting for an
	 *         acknowledgment.</i>, so if you put invalid exchange, it will not
	 *         throw error nor return false. If no exchange found, this will returns
	 *         true but you will get log error on logback.
	 * @throws IOException if publishing fail (e.g. rabbit server down)
	 */
	public boolean publish(String exchange, String routingKey, String message) throws IOException {
		if (!exchangeExists(exchange)) {
			throw new IOException("Exchange not exists : " + exchange);
		}

		rabbitTemplate.convertAndSend(exchange, routingKey, message);

		return true;
	}

	private boolean exchangeExists(String exchange) {
		var exists = rabbitTemplate.execute(new ChannelCallback<DeclareOk>() {
			@Override
			public DeclareOk doInRabbit(Channel channel) throws Exception {
				try {
					return channel.exchangeDeclarePassive(exchange);
				} catch (Exception e) {
					// not exists
					return null;
				}
			}
		});

		return exists != null;
	}

	/**
	 * 
	 * @param o
	 * @return
	 * @throws IOException if o is String, but not a valid json format
	 */
	public boolean publishAsJson(String exchange, String routingKey, Object o) throws IOException {
		if (o instanceof String) {
			// validate it's a valid json
			String objString = o.toString();
			objectMapper.readTree(objString);
			return publish(exchange, routingKey, objString);
		} else {
			return publish(exchange, routingKey, objectMapper.writeValueAsString(o));
		}
	}

}
