package com.jaeger.novemberajax.util;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {

	private static final ObjectMapper objectMapper = new ObjectMapper();

	public static boolean isValidJson(String string) {
		try {
			objectMapper.readTree(string);
			return true;
		} catch (IOException e) {
			return false;
		}
	}

}
