package com.jaeger.novemberajax.rest;

import java.util.ArrayList;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.jaeger.connpod.rest.json.JsonBaseError;
import com.jaeger.connpod.rest.json.JsonBaseResponse;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class JaegerResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		long start = System.currentTimeMillis();

		final var errors = new ArrayList<JsonBaseError>();
		final JsonBaseError jsonBaseError = new JsonBaseError("Cannot read body", ex.getMessage(), "99");
		errors.add(jsonBaseError);

		final JsonBaseResponse<String> response = new JsonBaseResponse<String>(start, false, errors);

		return handleExceptionInternal(ex, response, headers, HttpStatus.BAD_REQUEST, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		long start = System.currentTimeMillis();

		final var errors = new ArrayList<JsonBaseError>();
		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			final JsonBaseError jsonBaseError = new JsonBaseError("Bad request parameter",
					error.getField() + ": " + error.getDefaultMessage(), "99");
			errors.add(jsonBaseError);
		}
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			final JsonBaseError jsonBaseError = new JsonBaseError("Bad request parameter",
					error.getObjectName() + ": " + error.getDefaultMessage(), "99");
			errors.add(jsonBaseError);
		}

		final JsonBaseResponse<String> response = new JsonBaseResponse<String>(start, false, errors);

		return handleExceptionInternal(ex, response, headers, HttpStatus.BAD_REQUEST, request);
	}

	@Override
	protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		long start = System.currentTimeMillis();

		final var errors = new ArrayList<JsonBaseError>();
		final JsonBaseError jsonBaseError = new JsonBaseError("Bad request",
				"Missing path variable : " + ex.getVariableName(), "99");
		errors.add(jsonBaseError);

		final JsonBaseResponse<String> response = new JsonBaseResponse<String>(start, false, errors);

		return handleExceptionInternal(ex, response, headers, HttpStatus.BAD_REQUEST, request);
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		long start = System.currentTimeMillis();

		final var errors = new ArrayList<JsonBaseError>();
		final JsonBaseError jsonBaseError = new JsonBaseError("Bad request",
				"Missing request parameter : " + ex.getParameterName(), "99");
		errors.add(jsonBaseError);

		final JsonBaseResponse<String> response = new JsonBaseResponse<String>(start, false, errors);

		return handleExceptionInternal(ex, response, headers, HttpStatus.BAD_REQUEST, request);
	}

}
