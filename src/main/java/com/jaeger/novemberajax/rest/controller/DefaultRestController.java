package com.jaeger.novemberajax.rest.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = { "Health Check" }, description = "Health check / ping API")
@RestController
public class DefaultRestController {

	@ApiOperation(value = "Check server status", notes = "Check server status, will return 200 with simple string if alive. Do nothing else.", produces = MediaType.TEXT_PLAIN_VALUE)
	@GetMapping(value = { "/", "ping", "/health" })
	public String ping() {
		return "OK";
	}

}
