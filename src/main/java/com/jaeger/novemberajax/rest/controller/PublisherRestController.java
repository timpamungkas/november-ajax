package com.jaeger.novemberajax.rest.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jaeger.connpod.rest.builder.JsonResponseBuilder;
import com.jaeger.connpod.rest.json.JsonBaseResponse;
import com.jaeger.novemberajax.service.impl.RabbitmqPublisher;
import com.jaeger.novemberajax.util.JsonUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Publisher" }, description = "Pub-sub queue related API")
@RestController
public class PublisherRestController {

	private static final String KEY_PUBLISHED = "published";

	@Autowired
	private RabbitmqPublisher rabbitmqPublisher;

	@ApiOperation(value = "Publish to RabbitMQ", notes = "<p>Publish message to queue (RabbitMQ implementation).</p>"
			+ "<p>Since this API using RabbitMQ implementation, it requires <code>exchange</code> "
			+ "and <code>routingKey</code> defined</p>")
	@ApiResponses({
			@ApiResponse(code = 202, message = "Request body accepted and entering queue, waiting to be processed.") })
	@PostMapping(value = "/v1/publish/{exchange}/{routingKey}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JsonBaseResponse<Map<String, String>>> publish(@PathVariable("exchange") String exchange,
			@PathVariable("routingKey") String routingKey, @RequestBody String message) {
		var builder = new JsonResponseBuilder<Map<String, String>>();
		var bodyData = new HashMap<String, String>();
		JsonBaseResponse<Map<String, String>> response;
		long start = System.currentTimeMillis();

		// check if message is valid json data
		if (!JsonUtil.isValidJson(message)) {
			bodyData.put(KEY_PUBLISHED, Boolean.FALSE.toString());
			response = builder.withData(bodyData).addError("Invalid body", "Requires valid JSON to be published", "40")
					.withSuccess(false).withProcessTimeMillis(System.currentTimeMillis() - start).build();

			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			// core business process
			var result = rabbitmqPublisher.publish(exchange, routingKey, message);
			// end core business process

			bodyData.put(KEY_PUBLISHED, Boolean.toString(result));
			response = builder.withData(bodyData).withSuccess(result)
					.withProcessTimeMillis(System.currentTimeMillis() - start).build();

			return new ResponseEntity<>(response, result ? HttpStatus.ACCEPTED : HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IOException e) {
			bodyData.put(KEY_PUBLISHED, Boolean.FALSE.toString());
			response = builder.withData(bodyData).addError("Server error", e.getClass() + " : " + e.getMessage(), "50")
					.withSuccess(false).withProcessTimeMillis(System.currentTimeMillis() - start).build();

			System.out.println(response);

			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
