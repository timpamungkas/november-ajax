package com.jaeger.novemberajax.rest.controller;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jaeger.connpod.util.LogUtil;
import com.jaeger.novemberajax.service.QrCodeGenerator;
import com.jaeger.novemberajax.service.QrCodeGenerator.ImageFormat;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Barcode / QR Code Generator" }, description = "Generate QR code")
@RestController
public class QrCodeRestController {

	@Autowired
	private QrCodeGenerator qrCodeGenerator;

	private static final Logger log = LoggerFactory.getLogger(QrCodeRestController.class);
	private static final LogUtil logUtil = new LogUtil();
	public static final int ALLOWABLE_SIZE_MIN = 10;
	public static final int ALLOWABLE_SIZE_MAX = 1000;

	@ApiOperation(value = "Generate QR Code with certain text, size, & image format (PNG / JPG). "
			+ "For field validations, see detailed description of each field.")
	@ApiResponses(value = { @ApiResponse(message = "QR Code generated as image", code = 200),
			@ApiResponse(message = "Something wrong on server", code = 500),
			@ApiResponse(message = "Bad query params", code = 400) })
	@GetMapping(value = "/v1/qr")
	@Cacheable(value = { "qrCodeCache" })
	public ResponseEntity<byte[]> generateQrCode(
			@ApiParam(allowEmptyValue = false, value = "Text to be generated") @RequestParam(value = "t", required = true) String text,
			@ApiParam(allowEmptyValue = true, value = "Size (width = height)", allowableValues = "range["
					+ ALLOWABLE_SIZE_MIN + "," + ALLOWABLE_SIZE_MAX
					+ "]", defaultValue = "100") @RequestParam(value = "s", defaultValue = "100", required = false) int size,
			@ApiParam(allowEmptyValue = true, allowableValues = "PNG, JPG, JPEG", defaultValue = "PNG") @RequestParam(value = "f", defaultValue = "PNG", required = false) String imageType) {
		long start = System.currentTimeMillis();
		byte[] data = null;
		HttpHeaders headers = new HttpHeaders();
		ImageFormat imageFormat = null;

		if (size < ALLOWABLE_SIZE_MIN || size > ALLOWABLE_SIZE_MAX) {
			// bad requests
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
			return new ResponseEntity<>(data, headers, HttpStatus.BAD_REQUEST);
		}

		if (StringUtils.isEmpty(imageType) || imageType.equalsIgnoreCase(ImageFormat.PNG.toString())) {
			imageFormat = ImageFormat.PNG;
		} else if (imageType.equalsIgnoreCase(ImageFormat.JPG.toString())
				|| imageType.equalsIgnoreCase(ImageFormat.JPEG.toString())) {
			imageFormat = ImageFormat.JPEG;
		} else {
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
			return new ResponseEntity<>(data, headers, HttpStatus.BAD_REQUEST);
		}

		try {
			data = qrCodeGenerator.getQrCode(text, size, imageFormat);

			headers.add(HttpHeaders.CONTENT_TYPE,
					(imageFormat.equals(ImageFormat.PNG) ? MediaType.IMAGE_PNG_VALUE : MediaType.IMAGE_JPEG_VALUE));
			headers.add(HttpHeaders.CACHE_CONTROL, "public,max-age=" + (60 * 60 * 24));

			return new ResponseEntity<>(data, headers, HttpStatus.OK);
		} catch (IOException e) {
			log.error(logUtil.generateLogText(System.currentTimeMillis() - start, e.getMessage(), "EXCEPTION"));
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
			return new ResponseEntity<>(data, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
