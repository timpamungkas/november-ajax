package com.jaeger.novemberajax.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jaeger.connpod.rest.builder.JsonResponseBuilder;
import com.jaeger.connpod.rest.json.JsonBaseError;
import com.jaeger.connpod.rest.json.JsonBaseResponse;
import com.jaeger.connpod.util.LogUtil;
import com.jaeger.novemberajax.rest.model.Email;
import com.jaeger.novemberajax.service.impl.RabbitmqPublisher;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import model.RestPlainString;
import model.RestValidList;

/**
 * Send email processing request to pub-sub queue. With RabbitMQ pub-sub, you
 * need to define several properties on <code>jaeger-dev.properties</code>:
 * 
 * <ul>
 * <li>exchange to send message:
 * <code>jaeger.november-ajax.rabbitmq.email.exchange.work.name</code>. Default
 * is <code>x.november-ajax.work</code></li>
 * <li>message routing key:
 * <code>jaeger.november-ajax.rabbitmq.email.routing-key</code>. Default is
 * <code>q.november-ajax.email</code></li>
 * </ul>
 * 
 * @author timpamungkas
 *
 */
@Api(tags = "Notifier", description = "Send email")
@RestController
@RequestMapping("/v1/email")
public class EmailRestController {

	private static final Logger log = LoggerFactory.getLogger(EmailRestController.class);
	private static final int MAX_EMAIL_PAYLOAD = 20;

	@Value("${jaeger.november-ajax.rabbitmq.email.exchange.work.name:x.november-ajax.work}")
	private String exchangeNovemberAjax;

	@Value("${jaeger.november-ajax.rabbitmq.email.routing-key:q.november-ajax.email}")
	private String routingNovemberAjax_Email;

	private final LogUtil logUtils = new LogUtil();

	@Autowired
	private RabbitmqPublisher rabbitmqPublisher;

	/**
	 * Multi send. Will send each email to single recipient (to). Bcc and cc will be
	 * send as-is (can be multiple ccs/bccs in one email). If you have 1 email with
	 * 10 to addresses, 5 cc addresses, and 3 bcc addresses, this endpoint will send
	 * 10 email, one email to each to + 5 cc + 3 bcc.
	 * 
	 * @param email   Email to blast, with maximum is 100 email address for each
	 *                blast.
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Blast email", notes = "Send email blast where each email only has single 'to'."
			+ " Will send each email to single recipient ('to'). Note that 'bcc' and 'cc' will be"
			+ "	send as-is (can be multiple ccs/bccs in one email). If you have 1 email with"
			+ "	10 to addresses, 5 cc addresses, and 3 bcc addresses, this endpoint will send"
			+ "	10 email, one email to each to + 5 cc + 3 bcc, with same subject & body. "
			+ " To achieve endpoint speed, this endpoint will actually publish mail send request to"
			+ " queue, so it's not realtime send. Can send <strong>maximum " + Email.MAX_EMAIL_NUM_RECIPIENT
			+ " recipients for each 'to' / 'cc' / 'bcc'.</strong>")
	@ApiResponses({ @ApiResponse(code = 200, message = "Email request on queue to send"),
			@ApiResponse(code = 400, message = "Bad email request (e.g: invalid recipient address, empty subject") })
	@PostMapping(path = "/blast", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JsonBaseResponse<RestPlainString>> blastEmail(@Valid @RequestBody Email email,
			HttpServletRequest request) {
		log.info(logUtils.generateLogText(0, request.getRequestURI(), "START"));
		final long start = System.currentTimeMillis();
		final RestPlainString dataContent = new RestPlainString();
		HttpStatus httpStatus;
		boolean isHasError = false;

		List<JsonBaseError> errors = new ArrayList<>();
		Email splitMail = new Email(email.getTos(), email.getCcs(), email.getBccs(), email.getSubject(),
				email.getHtmlContent());

		// publish to pubsub
		if (email.ableToSend()) {
			// split email by to, convert to set (avoid double blast)
			Set<String> targetBlast = new HashSet<>(email.getTos());

			for (String to : targetBlast) {
				try {
					splitMail.setTos(Arrays.asList(to));

					// publish to rabbit
					rabbitmqPublisher.publishAsJson(exchangeNovemberAjax, routingNovemberAjax_Email, splitMail);
				} catch (IOException e) {
					isHasError = true;
					JsonBaseError error = new JsonBaseError();
					error.setMessage("Failed to send email to: " + email.getTos());
					error.setReason(e.getMessage());
					error.setCode(e.getClass().getName());

					errors.add(error);
				}
			}
		} else {
			// not OK
			httpStatus = HttpStatus.BAD_REQUEST;

			dataContent.setString(email.badEmailMessage());
			final long processTimeMillis = System.currentTimeMillis() - start;
			JsonBaseResponse<RestPlainString> response = new JsonResponseBuilder<RestPlainString>().withSuccess(false)
					.addError("Invalid email", "Bad email request (bad subject, invalid email address, ...)",
							Integer.toString(httpStatus.value()))
					.withData(dataContent).withProcessTimeMillis(processTimeMillis).build();

			ResponseEntity<JsonBaseResponse<RestPlainString>> jsonResult = new ResponseEntity<>(response, httpStatus);

			log.info(logUtils.generateLogText(processTimeMillis, request.getRequestURI(), "END"));
			return jsonResult;
		}

		// all OK
		if (isHasError) {
			// not OK
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			dataContent.setString("Failed publish to queue:");
			final long processTimeMillis = System.currentTimeMillis() - start;
			JsonBaseResponse<RestPlainString> response = new JsonResponseBuilder<RestPlainString>().withSuccess(false)
					.withData(dataContent).withProcessTimeMillis(processTimeMillis).withErrors(errors).build();

			ResponseEntity<JsonBaseResponse<RestPlainString>> jsonResult = new ResponseEntity<>(response, httpStatus);

			log.info(logUtils.generateLogText(processTimeMillis, request.getRequestURI(), "EXCEPTION"));
			return jsonResult;
		} else {
			httpStatus = HttpStatus.OK;
			dataContent.setString("Published to queue");
			final long processTimeMillis = System.currentTimeMillis() - start;
			JsonBaseResponse<RestPlainString> response = new JsonResponseBuilder<RestPlainString>().withSuccess(true)
					.withData(dataContent).withProcessTimeMillis(processTimeMillis).build();

			ResponseEntity<JsonBaseResponse<RestPlainString>> jsonResult = new ResponseEntity<>(response, httpStatus);
			log.info(logUtils.generateLogText(processTimeMillis, request.getRequestURI(), "END"));

			return jsonResult;
		}
	}

	/**
	 * Email recipient will be sent as-is. If you have 1 email with 10 to addresses,
	 * 5 cc addresses, and 3 bcc addresses, this endpoint will send one email to
	 * those 18 recipients (according the role).
	 * 
	 * @param emails List of emails in json, max of 10 distinct email (can be
	 *               different body, to, ...) per hit.
	 * @return
	 */
	@ApiOperation(value = "Send email", notes = "."
			+ " Will send each email to recipient as-is. Note that bcc and cc will be"
			+ "	send as-is (can be multiple ccs/bccs in one email). If you have 1 email with"
			+ "	10 to addresses, 5 cc addresses, and 3 bcc addresses, this endpoint will send"
			+ "	1 email with 10 to & 5 cc & 3 bcc. "
			+ " To achieve endpoint response time, this endpoint will actually publish mail send request to"
			+ " queue, so it's not realtime send. Can only send <strong>maximum " + MAX_EMAIL_PAYLOAD
			+ " email</strong> " + "per request.")
	@ApiResponses({ @ApiResponse(code = 200, message = "Email request on queue to send"),
			@ApiResponse(code = 400, message = "Bad email request (e.g: invalid recipient address, empty subject") })
	@PostMapping(path = "/send", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JsonBaseResponse<RestPlainString>> sendEmail(@Valid @RequestBody RestValidList<Email> emails,
			HttpServletRequest request) {
		log.info(logUtils.generateLogText(0, request.getRequestURI(), "START"));
		final long start = System.currentTimeMillis();
		final RestPlainString dataContent = new RestPlainString();
		HttpStatus httpStatus;
		boolean isHasError = false;

		if (emails.size() > MAX_EMAIL_PAYLOAD) {
			// not OK
			httpStatus = HttpStatus.BAD_REQUEST;
			String badRequest = "Too many email payload (max " + MAX_EMAIL_PAYLOAD + ", requested " + emails.size()
					+ ")";
			dataContent.setString(badRequest);
			final long processTimeMillis = System.currentTimeMillis() - start;
			JsonBaseResponse<RestPlainString> response = new JsonResponseBuilder<RestPlainString>().withSuccess(false)
					.addError("Invalid email", badRequest, Integer.toString(httpStatus.value())).withData(dataContent)
					.withProcessTimeMillis(processTimeMillis).build();

			ResponseEntity<JsonBaseResponse<RestPlainString>> jsonResult = new ResponseEntity<>(response, httpStatus);

			log.info(logUtils.generateLogText(processTimeMillis, request.getRequestURI(), "END"));
			return jsonResult;
		}

		List<JsonBaseError> errors = new ArrayList<>();

		// publish to pubsub
		for (Email email : emails) {
			try {
				if (email.ableToSend()) {
					// publish to rabbit
					rabbitmqPublisher.publishAsJson(exchangeNovemberAjax, routingNovemberAjax_Email, email);
				} else {
					throw new IllegalArgumentException(email.badEmailMessage());
				}
			} catch (Exception e) {
				isHasError = true;
				JsonBaseError error = new JsonBaseError();
				error.setMessage("Failed send email to: " + email.getTos());
				error.setReason(e.getMessage());
				error.setCode(e.getClass().getName());

				errors.add(error);

				continue;
			}
		}

		// all OK
		if (isHasError) {
			// not OK
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			dataContent.setString("Failed publish to queue:");
			final long processTimeMillis = System.currentTimeMillis() - start;
			JsonBaseResponse<RestPlainString> response = new JsonResponseBuilder<RestPlainString>().withSuccess(false)
					.withData(dataContent).withProcessTimeMillis(processTimeMillis).withErrors(errors).build();

			ResponseEntity<JsonBaseResponse<RestPlainString>> jsonResult = new ResponseEntity<>(response, httpStatus);

			log.info(logUtils.generateLogText(processTimeMillis, request.getRequestURI(), "EXCEPTION"));
			return jsonResult;
		} else {
			httpStatus = HttpStatus.OK;
			dataContent.setString("Published to queue");
			final long processTimeMillis = System.currentTimeMillis() - start;
			JsonBaseResponse<RestPlainString> response = new JsonResponseBuilder<RestPlainString>().withSuccess(true)
					.withData(dataContent).withProcessTimeMillis(processTimeMillis).build();

			ResponseEntity<JsonBaseResponse<RestPlainString>> jsonResult = new ResponseEntity<>(response, httpStatus);
			log.info(logUtils.generateLogText(processTimeMillis, request.getRequestURI(), "END"));

			return jsonResult;
		}
	}

}
