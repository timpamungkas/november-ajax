package com.jaeger.novemberajax.rest;

public interface JaegerHttpConstants {

	/** For exception message, used on LoggerInterceptor */
	String HEADER_EXCEPTION = "J-Exception";

	String PARAM_API_KEY = "key";

}
