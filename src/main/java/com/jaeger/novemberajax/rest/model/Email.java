package com.jaeger.novemberajax.rest.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Email {

	public static final int MAX_EMAIL_NUM_RECIPIENT = 100;

	@Size(max = MAX_EMAIL_NUM_RECIPIENT, message = "Max recipient is " + MAX_EMAIL_NUM_RECIPIENT)
	private List<String> bccs;

	@Size(max = MAX_EMAIL_NUM_RECIPIENT, message = "Max recipient is " + MAX_EMAIL_NUM_RECIPIENT)
	private List<String> ccs;

	@JsonProperty(value = "html_content")
	private String htmlContent;

	@NotBlank(message = "Subject must be set")
	@Size(max = 200, message = "Max length is 200 chars")
	@Valid
	private String subject;

	@Size(max = MAX_EMAIL_NUM_RECIPIENT, message = "Max recipient is " + MAX_EMAIL_NUM_RECIPIENT)
	private List<String> tos;

	// DONT rename this to getter (isValid / isAble), as Jackson will try to
	// deserialize it to field email (during kafka consume) and will cause error
	public boolean ableToSend() {
		EmailValidator emailValidator = EmailValidator.getInstance();

		if (StringUtils.isEmpty(subject) || CollectionUtils.isEmpty(tos) || tos.size() > MAX_EMAIL_NUM_RECIPIENT) {
			return false;
		}

		for (String to : tos) {
			if (!emailValidator.isValid(to)) {
				return false;
			}
		}

		if (CollectionUtils.isNotEmpty(ccs)) {
			if (ccs.size() > MAX_EMAIL_NUM_RECIPIENT) {
				return false;
			}

			for (String cc : ccs) {
				if (!emailValidator.isValid(cc)) {
					return false;
				}
			}
		}

		if (CollectionUtils.isNotEmpty(bccs)) {
			if (bccs.size() > MAX_EMAIL_NUM_RECIPIENT) {
				return false;
			}

			for (String bcc : bccs) {
				if (!emailValidator.isValid(bcc)) {
					return false;
				}
			}
		}

		return true;
	}

	public Email() {
		super();
	}

	public Email(List<String> tos, List<String> ccs, List<String> bccs, String subject, String htmlContent) {
		super();
		setTos(tos);
		setCcs(ccs);
		setBccs(bccs);
		setSubject(subject);
		setHtmlContent(htmlContent);
	}

	public List<String> getBccs() {
		return bccs;
	}

	/**
	 * Will set bccs, also remove empty string within parameter list.
	 * 
	 * @param bccs bccs list, will not validate email address or the email. To check
	 *             mail address, use method ableToSend.
	 */
	public void setBccs(List<String> bccs) {
		this.bccs = bccs == null ? new ArrayList<>() : bccs;
		this.bccs.removeIf(String::isEmpty);
	}

	public List<String> getCcs() {
		return ccs;
	}

	/**
	 * Will set ccs, also remove empty string within parameter list.
	 * 
	 * @param ccs ccs list, will not validate email address or the email. To check
	 *            mail address, use method ableToSend.
	 */
	public void setCcs(List<String> ccs) {
		this.ccs = ccs == null ? new ArrayList<>() : ccs;
		this.ccs.removeIf(String::isEmpty);
	}

	public String getHtmlContent() {
		return htmlContent;
	}

	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<String> getTos() {
		return tos;
	}

	/**
	 * Will set tos, also remove empty string within parameter list.
	 * 
	 * @param tos to list, will not validate email address or the email. To check
	 *            mail address, use method ableToSend.
	 */
	public void setTos(List<String> tos) {
		this.tos = tos == null ? new ArrayList<>() : tos;
		this.tos.removeIf(String::isEmpty);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bccs == null) ? 0 : bccs.hashCode());
		result = prime * result + ((ccs == null) ? 0 : ccs.hashCode());
		result = prime * result + ((htmlContent == null) ? 0 : htmlContent.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((tos == null) ? 0 : tos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Email other = (Email) obj;
		if (bccs == null) {
			if (other.bccs != null) {
				return false;
			}
		} else if (!bccs.equals(other.bccs)) {
			return false;
		}
		if (ccs == null) {
			if (other.ccs != null) {
				return false;
			}
		} else if (!ccs.equals(other.ccs)) {
			return false;
		}
		if (htmlContent == null) {
			if (other.htmlContent != null) {
				return false;
			}
		} else if (!htmlContent.equals(other.htmlContent)) {
			return false;
		}
		if (subject == null) {
			if (other.subject != null) {
				return false;
			}
		} else if (!subject.equals(other.subject)) {
			return false;
		}
		if (tos == null) {
			if (other.tos != null) {
				return false;
			}
		} else if (!tos.equals(other.tos)) {
			return false;
		}
		return true;
	}

	/**
	 * Generate error message for bad email (can't be sent due to
	 * {@link Email#ableToSend()} returns <code>false</code>).
	 * 
	 * @return
	 */
	public String badEmailMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("Can't send email. Valid email is : ");
		sb.append("has subject (yours: " + this.getSubject() + "). ");
		sb.append("All 'to' / 'cc' / 'bcc' recipients must valid email addresses. ");
		sb.append("max " + Email.MAX_EMAIL_NUM_RECIPIENT + " 'to' recipients (yours: "
				+ (this.getTos() != null ? this.getTos().size() : 0) + "), ");
		sb.append("max " + Email.MAX_EMAIL_NUM_RECIPIENT + " 'cc' recipients (yours: "
				+ (this.getCcs() != null ? this.getCcs().size() : 0) + "), ");
		sb.append("max " + Email.MAX_EMAIL_NUM_RECIPIENT + " 'bcc' recipients (yours: "
				+ (this.getBccs() != null ? this.getBccs().size() : 0) + "), ");

		return sb.toString();
	}

	@Override
	public String toString() {
		return "Email [bccs=" + bccs + ", ccs=" + ccs + ", htmlContent=" + htmlContent + ", subject=" + subject
				+ ", tos=" + tos + "]";
	}

}
