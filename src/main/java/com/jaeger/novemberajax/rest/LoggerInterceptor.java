package com.jaeger.novemberajax.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jaeger.connpod.util.LogUtil;

@Component
public class LoggerInterceptor extends HandlerInterceptorAdapter {

	private static final String REQUEST_ATTR_START_TIME = "startTime";

	private static Logger log = LoggerFactory.getLogger(LoggerInterceptor.class);

	private static LogUtil logUtil = new LogUtil();

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.info(logUtil.generateLogText(0, request.getRequestURI(), "START"));
		request.setAttribute(REQUEST_ATTR_START_TIME, System.currentTimeMillis());

		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		var startTime = (long) request.getAttribute(REQUEST_ATTR_START_TIME);
		String position = is2xxSuccessful(response.getStatus()) ? "END" : "EXCEPTION";
		String exception = response.getHeader(JaegerHttpConstants.HEADER_EXCEPTION);

		log.info(logUtil.generateLogText((System.currentTimeMillis() - startTime), request.getRequestURI(), position,
				exception));
	}

	private boolean is2xxSuccessful(int status) {
		return status >= 200 && status < 300;
	}

}
