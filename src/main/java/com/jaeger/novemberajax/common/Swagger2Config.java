package com.jaeger.novemberajax.common;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@ComponentScan(basePackages = { "com.jaeger.novemberajax.rest" })
public class Swagger2Config {

	private ApiInfo buildApiInfo() {
		Contact contact = new Contact("Timotius Pamungkas", "https://gitlab.com/timpamungkas",
				"timotius.pamungkas@gmail.com");

		return new ApiInfo("November-ajax API",
				"<code>november-ajax</code> is various technical tools to share, "
						+ "that needs high performance but simple functionalities.",
				"0.1.0", "", contact, "", "", Collections.emptyList());
	}

	@Bean
	public Docket swaggerApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build().apiInfo(buildApiInfo());
	}

}
