package com.jaeger.novemberajax.common.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

	@Autowired
	private HandlerInterceptor loggerInterceptor;

	@Autowired
	private JaegerAuthenticationEntryPoint authEntryPoint;

	@Autowired
	@Qualifier("jaegerSecurityDatasource")
	private DataSource dataSource;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(loggerInterceptor).excludePathPatterns("/webjars/**", "/swagger-resources/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// p is for [p]ublic
		http.csrf().disable().authorizeRequests().antMatchers(HttpMethod.POST).fullyAuthenticated()
				.antMatchers(HttpMethod.PUT).fullyAuthenticated().and().httpBasic()
				.authenticationEntryPoint(authEntryPoint).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// p is for [p]ublic
		web.ignoring().antMatchers("/", "/health", "/ping", "/p/**");
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// demo user (dev / password)
		// haseh password using bcrypt
		// (http://www.devglan.com/online-tools/bcrypt-hash-generator)
		//
		// auth.inMemoryAuthentication().withUser("dev")
		// .password("$2a$04$ur8RiekgJi/toXf69Idql.dNV./C9yCKzuT3wDDkdrAQKBpKZ.tyi").roles("USER",
		// "DEV");

		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("SELECT username, password, enabled FROM jagr_users where username = ?")
				.authoritiesByUsernameQuery("SELECT u.username, r.role FROM jagr_users u, jagr_user_roles r "
						+ "WHERE u.user_id = r.user_id " + " AND u.username = ?");
	}

	@Bean(value = "jaegerSecurityDatasource")
	@ConfigurationProperties("jaeger.security.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	// required for spring boot 2
	// password hash is bcrypt hash
	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

}
